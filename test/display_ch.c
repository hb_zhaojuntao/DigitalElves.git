#include <rtthread.h>
#include <rtdevice.h>
//添加文件操作使用需要的头文件
#include <dfs_posix.h>
//添加 lcd 显示使用的头文件
#include "drv_lcd.h"
#include "ascii.h"

#include <webclient.h>
#include <string.h>
#include "cJSON_util.h"

#if defined RT_USING_LCD_TEST
#define macWIDTH_EN_CHAR 8   //英文字符宽度
#define macHEIGHT_EN_CHAR 16 //英文字符高度

#define macWIDTH_CH_CHAR 16  //中文字符宽度
#define macHEIGHT_CH_CHAR 16 //中文字符高度

#define mac_DispWindow_X_Star 0 //  起始点的 X 坐标
#define mac_DispWindow_Y_Star 0 //  起始点的 Y 坐标

#define mac_DispWindow_COLUMN 240                   // 总列数
#define mac_DispWindow_PAGE 240                     //总行数
static const char *file_location = "/sd/HZLIB.bin"; //字库文件的路径
/**
 *  LCD 上显示英文字符
 * usX : 在特定扫描的方向下字符的起始 X 坐标
 * usY : 在特定扫描的方向下字符的起始 Y 坐标
 * cChar : 要显示的英文字符
 * usColor_Background : 选择英文字符的背景色
 * usColor_Foreground : 选择英文字符的前景色
 * 返回值 ： 无
 */
void lcd_disp_char_en(uint16_t usX, uint16_t usY, const char cChar, uint16_t usColor_Background, uint16_t usColor_Foreground)
{
    uint8_t ucTemp, ucRelativePosition, ucPage, ucColum;

    ucRelativePosition = cChar - ' ';

    lcd_open_window(usX, usY, macWIDTH_EN_CHAR, macHEIGHT_EN_CHAR);

    for (ucPage = 0; ucPage < macHEIGHT_EN_CHAR; ucPage++)
    {
        ucTemp = ucAscii_1608[ucRelativePosition][ucPage];

        for (ucColum = 0; ucColum < macWIDTH_EN_CHAR; ucColum++)
        {
            if (ucTemp & 0x01)
            {
                lcd_write_half_word(usColor_Foreground);
            }
            else
            {
                lcd_write_half_word(usColor_Background);
            }
            ucTemp >>= 1;

        } /*写完一行 */
    }
    /*全部写完 */
}

/**
 * 在 LCD 上显示英文字符串
 * usX : 在特定扫描方向下字符串的起始 X 坐标
 * usY : 在特定扫描方向下字符串的起始 Y 坐标
 * pStr : 要显示的英文字符串的首地址
 * usColor_Background : 选择英文字符串的背景色
 * usColor_Foreground : 选择英文字符串的前景色
 * 返回值 : 无
 */

void lcd_disp_str_en(uint16_t usX, uint16_t usY, const char *pStr, uint16_t usColor_Background, uint16_t usColor_Foreground)
{
    while (*pStr != '\0')
    {
        if ((usX - mac_DispWindow_X_Star + macWIDTH_EN_CHAR) > mac_DispWindow_COLUMN)
        {
            usX = mac_DispWindow_X_Star;
            usY += macHEIGHT_EN_CHAR;
        }

        if ((usY - mac_DispWindow_Y_Star + macHEIGHT_EN_CHAR) > mac_DispWindow_PAGE)
        {
            usX = mac_DispWindow_X_Star;
            usY = mac_DispWindow_Y_Star;
        }

        lcd_disp_char_en(usX, usY, *pStr, usColor_Background, usColor_Foreground);
        pStr++;
        usX += macWIDTH_EN_CHAR;
    }
}

/**
 * 从 SD 卡中获取中文字库
 * *pBuffer : 数据指针 
 * c : 读取的字符
 */

int GetGBKCode_from_sd(uint8_t *pBuffer, uint16_t c)
{
    unsigned char Hight8bit, Low8bit;
    unsigned int pos;
    int fd, size, ret;
    int result = 0;

    Hight8bit = c >> 8;   /* 取高8位数据 */
    Low8bit = c & 0x00FF; /* 取低8位数据 */

    //rt_kprintf("%d,%d\r\n",Hight8bit,Low8bit);
    //rt_kprintf("%x,%x\r\n",Hight8bit,Low8bit);

    pos = ((Hight8bit - 0xa0 - 16) * 94 + Low8bit - 0xa0 - 1) * 2 * 16;

    /* 以只读模式打开 字库文件 */
    fd = open(file_location, O_RDONLY);
    if (fd >= 0)
    {
        //移动指针
        ret = lseek(fd, pos, 0);
        //rt_kprintf("lseek  = %d \n",ret);
        size = read(fd, pBuffer, 32);
        close(fd);
        if (size < 0)
        {
            rt_kprintf("read %s  file error!!!\n", file_location);
        }

        result = 0;
    }
    else
    {
        rt_kprintf("open %s file error!!!\n", file_location);
        result = -1;
    }
    return result;
}

/**
 * 在 lcd 上显示中文字符
 * x : 在特定扫描方向下字符串的起始 X 坐标
 * y : 在特定扫描方向下字符串的起始 Y 坐标
 * usChar : 要显示的字符
 * usColor_Background : 选择字符串的背景色
 * usColor_Foreground : 选择字符串的前景色
 * 
 */
void lcd_disp_char_ch(uint16_t x, uint16_t y, uint16_t usChar, uint16_t usColor_Background, uint16_t usColor_Foreground)
{
    uint8_t ucPage, ucColum;
    uint8_t ucBuffer[32];
    uint16_t us_Temp;

    lcd_open_window(x, y, macWIDTH_CH_CHAR, macHEIGHT_CH_CHAR);
    GetGBKCode_from_sd(ucBuffer, usChar); //取字模数据

    for (ucPage = 0; ucPage < macHEIGHT_CH_CHAR; ucPage++)
    {
        //取出两个字节的数据，在 LCD 上即是一个汉字的一行
        us_Temp = ucBuffer[ucPage * 2];
        us_Temp = (us_Temp << 8);
        us_Temp |= ucBuffer[ucPage * 2 + 1];

        for (ucColum = 0; ucColum < macWIDTH_CH_CHAR; ucColum++)
        {
            if (us_Temp & (0x01 << 15)) //高位在前
            {
                lcd_write_half_word(usColor_Foreground);
            }
            else
            {
                lcd_write_half_word(usColor_Background);
            }
            us_Temp <<= 1;
        }
    }
}

/*
 * LCD 显示中文字符串
 * usX : 在特定扫描方向下字符串的起始 X 坐标
 * usY : 在特定扫描方向下字符串的起始 Y 坐标
 * pStr : 要显示的字符串首地址
 * usColor_Background : 选择字符串的背景色
 * usColor_Foreground : 选择字符串的前景色
 */
void lcd_disp_str_ch(uint16_t usX, uint16_t usY, const uint8_t *pStr, uint16_t usColor_Background, uint16_t usColor_Foreground)
{
    rt_uint16_t usCh = 0;
    rt_uint8_t usCh_low = 0;
    rt_uint8_t usCh_high = 0;
    rt_uint16_t usCh_temp = 0xffff;

    while (*pStr != '\0')
    {
        if ((usX - mac_DispWindow_X_Star + macWIDTH_CH_CHAR) > mac_DispWindow_COLUMN)
        {
            usX = mac_DispWindow_X_Star;
            usY += macHEIGHT_CH_CHAR;
        }

        if ((usY - mac_DispWindow_Y_Star + macHEIGHT_CH_CHAR) > mac_DispWindow_PAGE)
        {
            usX = mac_DispWindow_X_Star;
            usY = mac_DispWindow_Y_Star;
        }
        usCh_low = *pStr;
        usCh_high = *(pStr + 1);
        usCh_temp = 0xffff & usCh_low;
        usCh_temp = usCh_temp << 8;
        usCh_temp = usCh_temp | usCh_high;

        lcd_disp_char_ch(usX, usY, usCh_temp, usColor_Background, usColor_Foreground);
        usX += macWIDTH_CH_CHAR;
        pStr += 2; //一个汉字两个字节
    }
}

/**
 * 显示中英文字符串
 * usX : 在特定扫描方向下字符串的起始 X 坐标
 * usY : 在特定扫描方向下字符串的起始 Y 坐标
 * pStr : 要显示的字符串首地址
 * usColor_Background : 选择字符串的背景色
 * usColor_Foreground : 选择字符串的前景色
 * 
 * 
*/
void lcd_disp_str_en_ch(uint16_t usX, uint16_t usY, const uint8_t *pStr, uint16_t usColor_Background, uint16_t usColor_Foreground)
{
    uint16_t usCh;
    rt_uint8_t usCh_low = 0;
    rt_uint8_t usCh_high = 0;
    rt_uint16_t usCh_temp = 0xffff;
    while (*pStr != '\0')
    {
        if (*pStr <= 126) //英文字符
        {
            if ((usX - mac_DispWindow_X_Star + macWIDTH_EN_CHAR) > mac_DispWindow_COLUMN)
            {
                usX = mac_DispWindow_X_Star;
                usY += macHEIGHT_EN_CHAR;
            }

            if ((usY - mac_DispWindow_Y_Star + macHEIGHT_EN_CHAR) > mac_DispWindow_PAGE)
            {
                usX = mac_DispWindow_X_Star;
                usY = mac_DispWindow_Y_Star;
            }

            lcd_disp_char_en(usX, usY, *pStr, usColor_Background, usColor_Foreground);

            usX += macWIDTH_EN_CHAR;

            pStr++;
        }
        else
        {
            if ((usX - mac_DispWindow_X_Star + macWIDTH_CH_CHAR) > mac_DispWindow_COLUMN)
            {
                usX = mac_DispWindow_X_Star;
                usY += macHEIGHT_CH_CHAR;
            }

            if ((usY - mac_DispWindow_Y_Star + macHEIGHT_CH_CHAR) > mac_DispWindow_PAGE)
            {
                usX = mac_DispWindow_X_Star;
                usY = mac_DispWindow_Y_Star;
            }

            usCh_low = *pStr;
            usCh_high = *(pStr + 1);
            usCh_temp = 0xffff & usCh_low;
            usCh_temp = usCh_temp << 8;
            usCh_temp = usCh_temp | usCh_high;

            lcd_disp_char_ch(usX, usY, usCh_temp, usColor_Background, usColor_Foreground);
            usX += macWIDTH_CH_CHAR;
            pStr += 2; //一个汉字两个字节
        }
    }
}

int test_lcd_ch(int argc, char *argv[])
{
    rt_err_t ret = RT_EOK;
    lcd_disp_str_ch(0, 50, "小而美的", WHITE, BLUE);
    lcd_disp_str_ch(0, 100, "物联网操作系统", BLACK, BLUE);
    lcd_disp_str_ch(0, 150, "小而美的物联网操作系统", GREEN, BLUE);
    lcd_disp_str_ch(100, 200, "一二三四五六七八九十", BLACK, WHITE);
    lcd_disp_str_ch(0, 0, "麻雀虽小五脏俱全", RED, BLUE);
    lcd_disp_str_en(0, 20, "Hello, World!!!--RT-Thread...", WHITE, BLACK);
    lcd_disp_str_en_ch(0, 170, "Hello,你好,小而美的物联网操作系统.", WHITE, BLACK);

    return ret;
}

MSH_CMD_EXPORT(test_lcd_ch, test_lcd_ch);

#endif

static void diy_go(void)
{
    rt_kprintf("diy_go_test\r\n");
}

MSH_CMD_EXPORT(diy_go,diy_go test);


#define GET_HEADER_BUFSZ        1024        //头部大小
#define GET_RESP_BUFSZ          1024        //响应缓冲区大小
#define GET_URL_LEN_MAX         256         //网址最大长度
#define GET_URI                 "http://www.weather.com.cn/data/sk/%s.html" //获取天气的 API
#define AREA_ID                 "101090701" //河北沧州地区 ID

/* 天气数据解析 */
void weather_data_parse(rt_uint8_t *data)
{
    uint8_t temp[100];
    cJSON *root = RT_NULL, *object = RT_NULL, *item = RT_NULL;

    root = cJSON_Parse((const char *)data);
    if (!root)
    {
        rt_kprintf("No memory for cJSON root!\n");
        return;
    }
    object = cJSON_GetObjectItem(root, "weatherinfo");

    item = cJSON_GetObjectItem(object, "city");
    rt_kprintf("\ncity    :%s ", item->valuestring);
    lcd_clear(BLACK);
    // rt_sprintf(temp,"城市:  %s",item->valuestring);
    // lcd_disp_str_en_ch(0,0,temp,BLACK,WHITE);

    item = cJSON_GetObjectItem(object, "temp");
    rt_kprintf("\ntemp    :%s ", item->valuestring);
    rt_sprintf(temp,"温度:  %s",item->valuestring);
    lcd_disp_str_en_ch(0,20,temp,BLACK,WHITE);

    item = cJSON_GetObjectItem(object, "WD");
    rt_kprintf("\nwd      :%s ", item->valuestring);

    item = cJSON_GetObjectItem(object, "WS");
    rt_kprintf("\nws      :%s ", item->valuestring);
    // rt_sprintf(temp,"风力:  %s",item->valuestring);
    // lcd_disp_str_en_ch(0,40,temp,BLACK,WHITE);

    item = cJSON_GetObjectItem(object, "SD");
    rt_kprintf("\nsd      :%s ", item->valuestring);
    rt_sprintf(temp,"湿度:  %s",item->valuestring);
    lcd_disp_str_en_ch(0,40,temp,BLACK,WHITE);

    item = cJSON_GetObjectItem(object, "time");
    rt_kprintf("\ntime    :%s \n", item->valuestring);

    item = cJSON_GetObjectItem(object, "AP");
    rt_kprintf("\nap      :%s ", item->valuestring);
    rt_sprintf(temp,"气压:  %s",item->valuestring);
    lcd_disp_str_en_ch(0,60,temp,BLACK,WHITE);

    item = cJSON_GetObjectItem(object, "WSE");
    rt_kprintf("\nwse      :%s ", item->valuestring);
    rt_sprintf(temp,"风力:  %s",item->valuestring);
    lcd_disp_str_en_ch(0,80,temp,BLACK,WHITE);

#if 0
    //获取系统时间
    time_t now;
    /* output current time */
    now = time(RT_NULL);
    rt_kprintf("%s", ctime(&now));
    lcd_disp_str_en_ch(0,100,ctime(&now),BLACK,WHITE);
#endif

    if (root != RT_NULL)
        cJSON_Delete(root);
}
void get_weather(int argc, char **argv)
{
    rt_uint8_t *buffer = RT_NULL;
    int resp_status;
    struct webclient_session *session = RT_NULL;
    char *weather_url = RT_NULL;
    int content_length = -1, bytes_read = 0;
    int content_pos = 0;
    char *city_name = rt_calloc(1,255);

    /* 为 weather_url 分配空间 */
    weather_url = rt_calloc(1, GET_URL_LEN_MAX);
    if (weather_url == RT_NULL)
    {
        rt_kprintf("No memory for weather_url!\n");
        goto __exit;
    }

    if(argc == 1)
    {
        strcpy(city_name, AREA_ID);
    }
    else if (argc == 2)
    {
        strcpy(city_name, argv[1]);
    }

    /* 拼接 GET 网址 */
    rt_snprintf(weather_url, GET_URL_LEN_MAX, GET_URI, city_name);

    /* 创建会话并且设置响应的大小 */
    session = webclient_session_create(GET_HEADER_BUFSZ);
    if (session == RT_NULL)
    {
        rt_kprintf("No memory for get header!\n");
        goto __exit;
    }

    /* 发送 GET 请求使用默认的头部 */
    if ((resp_status = webclient_get(session, weather_url)) != 200)
    {
        rt_kprintf("webclient GET request failed, response(%d) error.\n", resp_status);
        goto __exit;
    }

    /* 分配用于存放接收数据的缓冲 */
    buffer = rt_calloc(1, GET_RESP_BUFSZ);
    if (buffer == RT_NULL)
    {
        rt_kprintf("No memory for data receive buffer!\n");
        goto __exit;
    }

    content_length = webclient_content_length_get(session);
    if (content_length < 0)
    {
        /* 返回的数据是分块传输的. */
        do
        {
            bytes_read = webclient_read(session, buffer, GET_RESP_BUFSZ);
            if (bytes_read <= 0)
            {
                break;
            }
        }while (1);
    }
    else
    {
        do
        {
            bytes_read = webclient_read(session, buffer,
                                        content_length - content_pos > GET_RESP_BUFSZ ?
                                        GET_RESP_BUFSZ : content_length - content_pos);
            if (bytes_read <= 0)
            {
                break;
            }
            content_pos += bytes_read;
        }while (content_pos < content_length);
    }

    /* 天气数据解析 */
    weather_data_parse(buffer);

__exit:
    /* 释放网址空间 */
    if (weather_url != RT_NULL)
        rt_free(weather_url);
    /* 关闭会话 */
    if (session != RT_NULL)
        webclient_close(session);
    /* 释放缓冲区空间 */
    if (buffer != RT_NULL)
        rt_free(buffer);
    if(city_name != RT_NULL)
        rt_free(city_name);
}

#ifdef FINSH_USING_MSH
#include <finsh.h>
MSH_CMD_EXPORT_ALIAS(get_weather, wt, wt [CityName]  webclient GET request test);
#endif /* FINSH_USING_MSH */


#define GET_FY2020
#ifdef GET_FY2020
#define GET_FY2020_HEADER_BUFSZ        (1024)        //头部大小
#define GET_FY2020_RESP_BUFSZ          (1024)       //响应缓冲区大小
#define GET_FY2020_URI         "http://www.dzyong.top:3005/yiqing/total" //疫情数据 API



/* 疫情数据解析 */
void fy2020_data_parse(rt_uint8_t *data)
{
    uint8_t temp[100];
    cJSON *root = RT_NULL, *object = RT_NULL, *item = RT_NULL;

    root = cJSON_Parse((const char *)data);
    if (!root)
    {
        rt_kprintf("No memory for cJSON root!\n");
        return;
    }

    cJSON *dataArray = cJSON_GetObjectItem(root,"data");  //取数组
    int arraySize = cJSON_GetArraySize(dataArray);        //取数组大小
    cJSON *dataList = dataArray->child;
    while(dataList != RT_NULL)
    {
        rt_kprintf("\ndiagnosed    :%d \n", cJSON_GetObjectItem(dataList,"diagnosed")->valueint);
        rt_kprintf("\ndeath    :%d \n", cJSON_GetObjectItem(dataList,"death")->valueint);
        rt_kprintf("\ncured    :%d \n", cJSON_GetObjectItem(dataList,"cured")->valueint);
        rt_kprintf("\ndate    :%s \n", cJSON_GetObjectItem(dataList,"date")->valuestring);
        //LCD屏打印信息
        rt_sprintf(temp,"累计确诊:  %d",cJSON_GetObjectItem(dataList,"diagnosed")->valueint);
        lcd_disp_str_en_ch(0,120,temp,BLACK,WHITE);
        rt_sprintf(temp,"累计死亡:  %d",cJSON_GetObjectItem(dataList,"death")->valueint);
        lcd_disp_str_en_ch(0,140,temp,BLACK,WHITE);
        rt_sprintf(temp,"累计治愈:  %d",cJSON_GetObjectItem(dataList,"cured")->valueint);
        lcd_disp_str_en_ch(0,160,temp,BLACK,WHITE);
        rt_sprintf(temp,"更新时间:  %s",cJSON_GetObjectItem(dataList,"date")->valuestring);
        lcd_disp_str_en_ch(0,180,temp,BLACK,WHITE);
        dataList = dataList->next;
    }

    




    //object = cJSON_GetObjectItem(root, "data");
    // item = cJSON_GetObjectItem(object, "data");
    // rt_kprintf("\ndata    :%s \n", item->valuestring);
    //lcd_clear(BLACK);
    // rt_sprintf(temp,"城市:  %s",item->valuestring);
    // lcd_disp_str_en_ch(0,0,temp,BLACK,WHITE);

    // item = cJSON_GetObjectItem(object, "temp");
    // rt_kprintf("\ntemp    :%s ", item->valuestring);
    // rt_sprintf(temp,"温度:  %s",item->valuestring);
    // lcd_disp_str_en_ch(0,20,temp,BLACK,WHITE);

    // item = cJSON_GetObjectItem(object, "WD");
    // rt_kprintf("\nwd      :%s ", item->valuestring);

    // item = cJSON_GetObjectItem(object, "WS");
    // rt_kprintf("\nws      :%s ", item->valuestring);
    // // rt_sprintf(temp,"风力:  %s",item->valuestring);
    // // lcd_disp_str_en_ch(0,40,temp,BLACK,WHITE);

    // item = cJSON_GetObjectItem(object, "SD");
    // rt_kprintf("\nsd      :%s ", item->valuestring);
    // rt_sprintf(temp,"湿度:  %s",item->valuestring);
    // lcd_disp_str_en_ch(0,40,temp,BLACK,WHITE);

    // item = cJSON_GetObjectItem(object, "time");
    // rt_kprintf("\ntime    :%s \n", item->valuestring);

    // //获取系统时间
    // time_t now;
    // /* output current time */
    // now = time(RT_NULL);
    // rt_kprintf("%s", ctime(&now));
    // lcd_disp_str_en_ch(0,60,ctime(&now),BLACK,WHITE);

    // if (root != RT_NULL)
    //     cJSON_Delete(root);
}
void get_fy2020_data(void)
{
    rt_uint8_t *buffer = RT_NULL;
    int resp_status;
    struct webclient_session *session = RT_NULL;
    int content_length = -1, bytes_read = 0;
    int content_pos = 0;
    int index;

    /* 创建会话并且设置响应的大小 */
    session = webclient_session_create(GET_FY2020_HEADER_BUFSZ);
    if (session == RT_NULL)
    {
        rt_kprintf("No memory for get header!\n");
        goto __exit;
    }

    /* 发送 GET 请求使用默认的头部 */
    if ((resp_status = webclient_get(session, GET_FY2020_URI)) != 200)
    {
        rt_kprintf("webclient GET request failed, response(%d) error.\n", resp_status);
        goto __exit;
    }

    /* 分配用于存放接收数据的缓冲 */
    buffer = rt_calloc(1, GET_FY2020_RESP_BUFSZ);
    if (buffer == RT_NULL)
    {
        rt_kprintf("No memory for data receive buffer!\n");
        goto __exit;
    }

    content_length = webclient_content_length_get(session);
    if (content_length < 0)
    {
         rt_kprintf("webclient GET request type is chunked.\n");
        /* 返回的数据是分块传输的. */
        do
        {
            bytes_read = webclient_read(session, buffer, GET_FY2020_RESP_BUFSZ);
            if (bytes_read <= 0)
            {
                break;
            }
            for (index = 0; index < bytes_read; index++)
            {
                rt_kprintf("%c", buffer[index]);
            }
        }while (1);
    }
    else
    {
        do
        {
            bytes_read = webclient_read(session, buffer,
                                        content_length - content_pos > GET_FY2020_RESP_BUFSZ ?
                                        GET_FY2020_RESP_BUFSZ : content_length - content_pos);
            if (bytes_read <= 0)
            {
                break;
            }
            content_pos += bytes_read;
        }while (content_pos < content_length);
    }

    for (index = 0; index < bytes_read; index++)
    {
        rt_kprintf("%c", buffer[index]);
    }
    /* 肺炎数据解析 */
    fy2020_data_parse(buffer);

__exit:
    /* 关闭会话 */
    if (session != RT_NULL)
        webclient_close(session);
    /* 释放缓冲区空间 */
    if (buffer != RT_NULL)
        rt_free(buffer);
}

#ifdef FINSH_USING_MSH
#include <finsh.h>
MSH_CMD_EXPORT(get_fy2020_data,get_fy2020_data);
#endif /* FINSH_USING_MSH */

#endif