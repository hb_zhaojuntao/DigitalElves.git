#ifndef _TJPGD_EXAMPLE_H_
#define _TJPGD_EXAMPLE_H_

int LCDShowWelcome(void);
int LCDShowMusic(void);
int Decode_Jpg(rt_uint8_t *file_name);

#endif
